﻿var taskName; //holds the value contained by elementID == 'taskName'
var estTime;  //holds the value contained bye elementID == 'estTime'
var taskArray = [];
var timeArray = [];

/*Initializes the application when the web page is loaded*/
function initialize()
{
    taskName = document.getElementById('taskName');
    estTime = document.getElementById('estTime');

    document.getElementById('addTask').addEventListener('click', clickAddTask, false);
}

/*Gets task name and estimated time
HTML form will validate that the form is not empty*/
function clickAddTask()
{
    //get task name
    taskName.value = document.getElementById('taskName').value;

    //get estimated time
    estTime.value = document.getElementById('estTime').value;

    //add task to list
    addTaskToList(taskName, estTime);

    //set input fields to empty
    taskName.value = "";
    estTime.value = "";
}

/*adds new tasks to a list that grows dynamically
  newer tasks are added at the beginning*/
function addTaskToList(taskName, estTime)
{
    taskArray.unshift(taskName.value); //adds task name to the beginning of the array
    timeArray.unshift(estTime.value);
    var taskLi = "<li>"; //creates a list element
    $('#taskList').prepend(taskLi.concat(taskArray[0]));
    var timeLi = "<li>";
    $("#taskList").prepend(taskLi.concat(taskArray[0])); //adds values to list
    $("#timeList").prepend(timeLi.concat(timeArray[0]));
}